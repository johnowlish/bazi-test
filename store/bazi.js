// import moment from 'moment'

const stems = [
  { id: 1, name: 'Jia', glyph: '甲', element: 'wood', yin_yang: false, com: 6, col: 7 },
  { id: 2, name: 'Yi', glyph: '乙', element: 'wood', yin_yang: true, com: 7, col: 8 },
  { id: 3, name: 'Bing', glyph: '丙', element: 'fire', yin_yang: false, com: 8, col: 9 },
  { id: 4, name: 'Ding', glyph: '丁', element: 'fire', yin_yang: true, com: 9, col: 10 },
  { id: 5, name: 'Wu', glyph: '戊', element: 'earth', yin_yang: false, com: 10, col: 0 },
  { id: 6, name: 'Ji', glyph: '己', element: 'earth', yin_yang: true, com: 1, col: 0 },
  { id: 7, name: 'Geng', glyph: '庚', element: 'metal', yin_yang: false, com: 2, col: 1 },
  { id: 8, name: 'Xin', glyph: '辛', element: 'metal', yin_yang: true, com: 3, col: 2 },
  { id: 9, name: 'Ren', glyph: '壬', element: 'water', yin_yang: false, com: 4, col: 3 },
  { id: 10, name: 'Gui', glyph: '癸', element: 'water', yin_yang: true, com: 5, col: 4 }
]
const branches = [
  { id: 1, name: 'Zi', glyph: '子', zodiac: 'rat', season: 'winter', dir: 'n', str: 'w', harm: 8, com: 2, col: 7 },
  { id: 2, name: 'Chou', glyph: '丑', zodiac: 'ox', season: 'winter', dir: 'ne', str: 'm', harm: 7, com: 1, col: 8 },
  { id: 3, name: 'Yin', glyph: '寅', zodiac: 'tiger', season: 'spring', dir: 'en', str: 'f', harm: 6, com: 12, col: 9 },
  { id: 4, name: 'Mao', glyph: '卯', zodiac: 'rabbit', season: 'spring', dir: 'e', str: 't', harm: 5, com: 11, col: 10 },
  { id: 5, name: 'Chen', glyph: '辰', zodiac: 'dragon', season: 'spring', dir: 'es', str: 'w', harm: 4, com: 10, col: 11 },
  { id: 6, name: 'Si', glyph: '巳', zodiac: 'snake', season: 'summer', dir: 'se', str: 'm', harm: 3, com: 9, col: 12 },
  { id: 7, name: 'Wu', glyph: '午', zodiac: 'horse', season: 'summer', dir: 's', str: 'f', harm: 2, com: 8, col: 1 },
  { id: 8, name: 'Wei', glyph: '未', zodiac: 'goat', season: 'summer', dir: 'sw', str: 't', harm: 1, com: 7, col: 2 },
  { id: 9, name: 'Shen', glyph: '申', zodiac: 'monkey', season: 'autumnn', dir: 'ws', str: 'w', harm: 12, com: 6, col: 3 },
  { id: 10, name: 'You', glyph: '酉', zodiac: 'rooster', season: 'autumn', dir: 'w', str: 'm', harm: 11, com: 5, col: 4 },
  { id: 11, name: 'Xu', glyph: '戌', zodiac: 'dog', season: 'autumn', dir: 'wn', str: 'f', harm: 10, com: 4, col: 5 },
  { id: 12, name: 'Hai', glyph: '亥', zodiac: 'pig', season: 'winter', dir: 'nw', str: 't', harm: 9, com: 3, col: 6 }
]
// const times = ['00 - 01', '01 - 03', '03 - 05', '05 - 07', '07 - 09', '09 - 11', '11 - 13',
//   '13 - 15', '15 - 17', '17 - 19', '19 - 21', '21 - 23', '23 - 00']
// const stages = [
//   { id: 1, name: 'Growth', ruName: 'Рождение' },
//   { id: 2, name: 'Bath', ruName: 'Купание' },
//   { id: 3, name: 'Young', ruName: 'Юность' },
//   { id: 4, name: 'Thriving', ruName: 'Чиновник' },
//   { id: 5, name: 'Prosperous', ruName: 'Процветание' },
//   { id: 6, name: 'Weak', ruName: 'Ослабление' },
//   { id: 7, name: 'Sick', ruName: 'Болезнь' },
//   { id: 8, name: 'Death', ruName: 'Смерть' },
//   { id: 9, name: 'Grave', ruName: 'Могила' },
//   { id: 10, name: 'Extinct', ruName: 'Исчесзновние' },
//   { id: 11, name: 'Conceive', ruName: 'Зачатие' },
//   { id: 12, name: 'Nurture', ruName: 'Вскармливание' }
// ]

export const state = () => ({
  year: {},
  prevYear: {},
  months: [],
  days: [],
  monthStarts: {}
})

export const actions = {
  async getYear ({ commit }, dateYear) {
    await this.$axios.$get(`/bazi/year/${dateYear}`)
      .then(year => commit('setYear', year))
  }
}

export const mutations = {
  setYear (state, year) {
    state.year = null
    state.year = {
      year: year.year,
      stem: Object.assign({}, stems.find(s => { return s.id === year.skdg.hs })),
      branch: Object.assign({}, branches.find(b => { return b.id === year.skdg.eb }))
    }
    state.year.stem.skdg = year.skdg.sh
    state.year.branch.skdg = year.skdg.se
    state.prevYear = {
      year: year.prev.year,
      stem: Object.assign({}, stems.find(s => { return s.id === year.prev.skdg.hs })),
      branch: Object.assign({}, branches.find(b => { return b.id === year.prev.skdg.eb }))
    }
    state.prevYear.stem.skdg = year.prev.skdg.sh
    state.prevYear.branch.skdg = year.prev.skdg.se
    state.months = []
    state.months = year.months.map(month => {
      let newMonth = {
        month: month.month,
        days: month.days,
        // jieDay: month.jieDay,
        stem: Object.assign({}, stems.find(s => { return s.id === month.skdg.hs })),
        branch: Object.assign({}, branches.find(b => { return b.id === month.skdg.eb }))
      }
      newMonth.stem.skdg = month.skdg.sh
      newMonth.branch.skdg = month.skdg.se
      return newMonth
    })
    state.days = year.days.map(day => {
      let newDay = {
        day: day.day,
        stem: Object.assign({}, stems.find(s => { return s.id === day.skdg.hs })),
        branch: Object.assign({}, branches.find(b => { return b.id === day.skdg.eb }))
      }
      newDay.stem.skdg = day.skdg.sh
      newDay.branch.skdg = day.skdg.se
      return newDay
    })
    state.monthStarts = year.monthStarts
  }
}
