export const state = () => ({
  date: new Date().toISOString().substr(0, 10),
  year: 0,
  months: [],
  month: 0,
  day: 0
})

export const actions = {
  setDate ({ commit }, date) {
    commit('SET_DATE', date)
  },
  nextDay ({ commit }) {
    commit('NEXT_DAY')
  },
  nextMonth ({ commit }) {
    commit('NEXT_MONTH')
  },
  nextYear ({ commit }) {
    commit('NEXT_YEAR')
  },
  prevDay ({ commit }) {
    commit('PREV_DAY')
  },
  prevMonth ({ commit }) {
    commit('PREV_MONTH')
  },
  prevYear ({ commit }) {
    commit('PREV_YEAR')
  }
}

export const mutations = {
  SET_DATE (state, date) {
    state.date = null
    state.date = date
    let y = parseInt(date.split('-')[0], 10)
    let m = parseInt(date.split('-')[1], 10)
    let d = parseInt(date.split('-')[2], 10)
    state.year = null
    state.year = y
    state.month = null
    state.month = m
    state.day = null
    state.day = d
  },
  NEXT_DAY (state) {
    let daysInMonth = new Date(state.year, state.month, 0).getDate()
    let y = state.year
    let m = state.month
    let d = state.day
    if (d === daysInMonth) {
      if (m === 12) {
        y++
        m = 1
        d = 1
      } else {
        m++
        d = 1
      }
    } else {
      d++
    }
    state.year = y
    state.month = m
    state.day = d
    state.date = `${y}-${m}-${d}`
  },
  NEXT_MONTH (state) {
    let y = state.year
    let m = state.month
    let d = state.day
    if (m === 12) {
      y++
      m = 1
    } else {
      m++
    }
    state.year = y
    state.month = m
    state.date = `${y}-${m}-${d}`
  },
  NEXT_YEAR (state) {
    let y = state.year
    let m = state.month
    let d = state.day
    y++
    state.year = y
    state.date = `${y}-${m}-${d}`
  },
  PREV_DAY (state) {
    let daysInMonth = new Date(state.year, state.month, 0).getDate()
    let y = state.year
    let m = state.month
    let d = state.day
    if (d === 1) {
      if (m === 1) {
        y--
        m = 12
        d = 31
      } else {
        m--
        d = daysInMonth
      }
    } else {
      d--
    }
    state.year = y
    state.month = m
    state.day = d
    state.date = `${y}-${m}-${d}`
  },
  PREV_MONTH (state) {
    let y = state.year
    let m = state.month
    let d = state.day
    if (m === 1) {
      y--
      m = 12
    } else {
      m--
    }
    state.year = y
    state.month = m
    state.date = `${y}-${m}-${d}`
  },
  PREV_YEAR (state) {
    let y = state.year
    let m = state.month
    let d = state.day
    y--
    state.year = y
    state.date = `${y}-${m}-${d}`
  }
}
