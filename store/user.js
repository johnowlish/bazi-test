// const user = JSON.parse(localStorage.getItem('user'))
// export const state = () => ({
//   status: {},
//   user: null
// })
//
// export const actions = {
//   login () {
//   },
//   logout () {
//   }
// }
//
// export const mutations = {
// }
export const getters = {
  isAuthenticated (state) {
    return state.auth.loggedIn
  },
  user (state) {
    return state.auth.user
  }
}
