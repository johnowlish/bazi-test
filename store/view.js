export const state = () => ({
  user: false,
  skdg: true,
  cols: false
})

export const actions = {
  USER ({ commit }) {
    commit('viewUser')
  },
  SKDG ({ commit }) {
    commit('viewSkdg')
  },
  COLS ({ commit }) {
    commit('viewCols')
  }
}

export const mutations = {
  viewUser (state) {
    state.user = !state.user
  },
  viewSkdg (state) {
    state.skdg = !state.skdg
  },
  viewCols (state) {
    state.cols = !state.cols
  }
}
// export const getters = {
//   skdg (state) {
//     return state.view.skdg
//   }
// }
